import uuid from 'react-native-uuid'

import { realm } from "./realm"
import { USER_SCHEMA } from "./allSchemas";

export const actions = {
    get: () => {
        return realm.objects(USER_SCHEMA)[0]
    },
    create: ({ name }) => {
        realm.write(() => {
            realm.create(USER_SCHEMA, {
                id: uuid.v4(), name
            })
        })
    }
}