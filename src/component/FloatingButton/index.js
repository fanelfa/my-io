import React from 'react'

import { TouchableOpacity } from "react-native"
import tw from 'tailwind-react-native-classnames'

import Icon from 'react-native-vector-icons/Feather'

const FloatingButton = ({
   onPress = () => { },
}) => {
   return <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={tw`w-12 h-12 rounded-xl absolute right-8 bottom-12 bg-blue-500 justify-center items-center shadow-lg`}
   >
      <Icon name="plus" style={tw`font-bold text-4xl text-white`} />
   </TouchableOpacity>
}

export default FloatingButton