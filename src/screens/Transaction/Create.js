import React from 'react'
import Form from '~screens/Transaction/Form';
import { actions } from 'services/db/Transaction';


const Create = ({ navigation }) => {
   const onSave = (data) => {
      if (typeof data === 'object') {
         actions.create({ ...data })
         navigation.goBack();
      }
   }
   return <Form onPressBack={() => navigation.goBack()} onSave={onSave} />
}

export default Create