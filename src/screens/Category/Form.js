import React, { useState } from 'react'
import { ScrollView, TextInput, TouchableOpacity, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import Text from '~component/Text';
import ViewM5 from '~component/ViewM5';
import { TRANSACTION_TYPE } from 'utility/GlobalConst';
import InputLabel from '~component/InputLabel';
import Icon from 'react-native-vector-icons/Feather';


const Form = ({
   title = 'Add Category',
   onSave = (data) => { },
   onPressBack = () => { },
}) => {
   const [name, setName] = useState('');
   const [isIncome, setIsIncome] = useState(true);

   const _onSave = () => {
      if (name.length > 2) {
         onSave({
            name,
            transactionType: isIncome ? TRANSACTION_TYPE.income : TRANSACTION_TYPE.expense,
         })
      }
   }

   return <View style={tw`flex-1`}>
      <View style={tw`bg-blue-500 h-64`}></View>
      <ScrollView style={{
         flex: 1,
         position: 'absolute',
         left: 0, right: 0, bottom: 0, top: 0
      }}>
         <ViewM5 style={tw`mt-8`}>
            <Text quicksandSemibold style={tw`text-3xl my-1 text-white`}>{title}</Text>
         </ViewM5>
         <ViewM5 style={tw`my-10 rounded-xl px-3 py-5 bg-white`}>
            <InputLabel name="Name" />
            <TextInput
               style={tw`border border-gray-400 rounded-lg px-5 text-lg mb-1 bg-white`}
               onChangeText={setName}
               value={name}
            />
            <InputLabel name="Transaction Type" />
            <View style={tw`flex-row`}>
               <TouchableOpacity
                  style={tw`flex-1 flex-row justify-center p-2 bg-white border-2 ${isIncome ? 'border-blue-400' : 'border-gray-300'} rounded-lg`}
                  onPress={() => setIsIncome(true)}
               >
                  {
                     isIncome && <Icon name="check-circle" style={tw`absolute left-3 top-2 text-xl text-blue-400 mr-3`} />
                  }
                  <Text quicksandSemibold style={tw`text-lg text-gray-600`}>Income</Text>
               </TouchableOpacity>
               <TouchableOpacity
                  style={tw`ml-3 flex-1 flex-row justify-center p-2 bg-white border-2 ${!isIncome ? 'border-blue-400' : 'border-gray-300'} rounded-lg`}
                  onPress={() => setIsIncome(false)}
               >
                  {
                     !isIncome && <Icon name="check-circle" style={tw`absolute left-3 top-2 text-xl text-blue-400 mr-3`} />
                  }
                  <Text quicksandSemibold style={tw`text-lg text-gray-600`}>Expense</Text>
               </TouchableOpacity>
            </View>
            <View style={tw`mt-14 mb-5 bg-white flex-row`}>
               <TouchableOpacity
                  activeOpacity={.7}
                  style={tw`p-2 flex-1 items-center bg-blue-500 rounded-lg`}
                  onPress={_onSave}
               >
                  <Text quicksandSemibold style={tw`text-xl text-white`}>Save</Text>
               </TouchableOpacity>
               <TouchableOpacity
                  activeOpacity={.5}
                  onPress={onPressBack}
                  style={tw`ml-3 p-2 flex-1 items-center bg-white rounded-lg border-2 border-gray-100`}
               >
                  <Text quicksandSemibold style={tw`text-xl text-gray-700`}>Cancel</Text>
               </TouchableOpacity>
            </View>
         </ViewM5>
      </ScrollView>
   </View>
}

export default Form