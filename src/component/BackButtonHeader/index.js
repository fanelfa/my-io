import { useNavigation } from '@react-navigation/core'
import React from 'react'
import { TouchableOpacity } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Icon from 'react-native-vector-icons/Feather'


const BackButtonHeader = () => {
    const navigation = useNavigation();

    return <TouchableOpacity
        style={tw`h-10 w-10 justify-center items-center rounded-lg bg-white absolute left-5 right-0 top-4 z-10 shadow`}
        onPress={() => navigation.goBack()}
    >
        <Icon name="chevron-left" style={tw`font-bold text-3xl text-gray-500 pr-1`} />
    </TouchableOpacity>
}

export default BackButtonHeader