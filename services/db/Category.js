import uuid from 'react-native-uuid'

import { openRealm, realm } from "./realm"
import { TRANSACTION_TYPE } from 'utility/GlobalConst';
import { CATEGORY_SCHEMA } from "./allSchemas";

const objectCategory = (name, isIncome = true) => ({
    id: uuid.v4(), name, transactionType: isIncome ? TRANSACTION_TYPE.income : TRANSACTION_TYPE.expense,
})

const initDataCategories = [
    objectCategory('Gift'),
    objectCategory('Salary'),
    objectCategory('Bonus'),
    objectCategory('Investment Returns'),
    objectCategory('Other Income'),
    objectCategory('Shoping', false),
    objectCategory('Eat and Drink', false),
    objectCategory('Education', false),
    objectCategory('Entertaintment', false),
    objectCategory('Investment', false),
    objectCategory('Holiday', false),
    objectCategory('Transportation', false),
    objectCategory('Health', false),
    objectCategory('Bill', false),
    objectCategory('Other Expense', false),
]

export const actions = {
    getAll: () => {
        return realm.objects(CATEGORY_SCHEMA)
    },
    initCategories: () => {
        const categories = realm.objects(CATEGORY_SCHEMA)
        realm.write(() => {
            if (!categories.length > 0) {
                initDataCategories.forEach(category => {
                    realm.create(CATEGORY_SCHEMA, category);
                })
            }
        })
    },
    create: ({ name, transactionType }) => {
        realm.write(() => {
            realm.create(CATEGORY_SCHEMA, {
                id: uuid.v4(), name, transactionType
            })
        })
    }
}