import React, { useState } from 'react'
import { Modal, ScrollView, TextInput, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import CurrencyInput from 'react-native-currency-input'
import DateTimePicker from '@react-native-community/datetimepicker';
import Text from '../Text'
import ViewM5 from '../ViewM5'


const InputLabel = ({ name = '' }) => <Text quicksandSemibold style={tw`text-base my-2 text-gray-500`}>{name}</Text>
const ModalAddTransaction = ({
   visible = false,
   onCancel = () => { },
}) => {

   const [isIncome, setIsIncome] = useState(true);
   const [total, setTotal] = useState();
   const [label, setLabel] = useState();
   const [date, setDate] = useState(new Date());
   const [showDatePicker, setShowDatePicker] = useState(false);

   const onChangeDate = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      setShowDatePicker(false);
   };

   return <Modal
      visible={visible} animationType="slide"
   >
      <View style={tw`mt-5 mb-2`}>
         <Text quicksandSemibold style={tw`text-2xl my-1 text-center`}>Add Transaction</Text>
      </View>
      <ScrollView style={{
         flex: 1,
         marginBottom: 74,
      }}>
         <ViewM5 style={tw`mt-20 mb-32`}>
            <InputLabel name="Type" />
            <View style={tw`flex-row`}>
               <TouchableOpacity
                  style={tw`flex-1 items-center p-2 bg-white border-2 ${isIncome ? 'border-green-300 bg-green-400' : 'border-gray-200'} rounded-lg`}
                  onPress={() => setIsIncome(true)}
               >
                  <Text quicksandSemibold style={tw`text-lg ${isIncome ? 'text-white' : 'text-black'}`}>Income</Text>
               </TouchableOpacity>
               <TouchableOpacity
                  style={tw`ml-3 flex-1 items-center p-2 bg-white border-2 ${!isIncome ? 'border-red-300 bg-red-400' : 'border-gray-200'} border-gray-200 rounded-lg`}
                  onPress={() => setIsIncome(false)}
               >
                  <Text quicksandSemibold style={tw`text-lg ${!isIncome ? 'text-white' : 'text-black'}`}>Expense</Text>
               </TouchableOpacity>
            </View>
            <InputLabel name="Label" />
            <TextInput
               placeholder="ex: Buy stock"
               style={tw`border rounded-lg px-5 text-lg mb-1 ${isIncome ? 'border-green-200 text-green-500' : 'border-red-200 text-red-500'}`}
               onChangeText={setLabel}
               value={label}
            />
            <InputLabel name="Total" />
            <CurrencyInput
               style={tw`border ${isIncome ? 'border-green-200 text-green-500' : 'border-red-200 text-red-500'} rounded-lg px-5 py-2 text-2xl`}
               value={total}
               onChangeValue={setTotal}
               precision={0}
               prefix="Rp "
               textAlign="right"
               minValue={0}
            />
            <InputLabel name="Date" />
            <TouchableOpacity
               style={[tw`border rounded-lg px-5 mb-1 ${isIncome ? 'border-green-200' : 'border-red-200'}`, { paddingVertical: 10 }]}
               onPress={() => setShowDatePicker(true)}
               activeOpacity={.8}
            >
               <Text style={tw`text-lg ${isIncome ? 'text-green-500' : 'text-red-500'}`} >
                  {new Date(date).toDateString()}
               </Text>
            </TouchableOpacity>
         </ViewM5>
      </ScrollView>
      <View style={tw`flex-1 p-3 bg-white flex-row shadow-xl absolute left-0 bottom-0 justify-start`}>
         <TouchableOpacity activeOpacity={.7} style={tw`p-2 flex-1 items-center bg-blue-500 rounded-lg`}>
            <Text quicksandSemibold style={tw`text-xl text-white`}>Simpan</Text>
         </TouchableOpacity>
         <TouchableOpacity
            activeOpacity={.5}
            onPress={onCancel}
            style={tw`ml-3 p-2 flex-1 items-center bg-white rounded-lg border border-gray-200`}
         >
            <Text quicksandSemibold style={tw`text-xl`}>Batal</Text>
         </TouchableOpacity>
      </View>
      {
         showDatePicker && <DateTimePicker
            value={date}
            mode="date"
            display="default"
            onChange={onChangeDate}
            maximumDate={new Date()}
         />
      }
   </Modal>
}

export default ModalAddTransaction