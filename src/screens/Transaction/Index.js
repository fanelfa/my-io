import { useIsFocused } from '@react-navigation/core';
import React, { Fragment, useEffect, useState } from 'react'
import { SafeAreaView, ScrollView, View } from 'react-native'
import { actions } from 'services/db/Transaction';
import tw from 'tailwind-react-native-classnames'
import { getDate } from 'utility/datetime';
import { groupBy } from 'utility/grouping';
import BackButtonHeader from '~component/BackButtonHeader';
import FloatingButton from '~component/FloatingButton';
import Text from '~component/Text';
import Transaction from '~component/Transaction/Item';
import ViewM5 from '~component/ViewM5';

const Index = ({ navigation }) => {
    const isFocused = useIsFocused()
    const [transactions, setTransactions] = useState([]);
    const [groups, setGroups] = useState({});

    useEffect(() => {
        const _transactions = (actions.getAll() || []).map(it => {
            it.groupDate = it.date.toLocaleDateString()
            return it
        })
        setGroups({
            ...groupBy(_transactions, 'groupDate')
        })
        setTransactions([..._transactions])
    }, [isFocused])

    const onPressItem = (item) => {
        navigation.navigate('TransactionUpdate', { item: JSON.stringify(item) });
    }

    return <SafeAreaView style={tw`flex-1`}>
        <View style={tw`w-64 h-64 rounded-full bg-blue-100 absolute -left-28 top-32`}></View>
        <BackButtonHeader />
        <ScrollView style={tw`flex-1`}>
            <ViewM5 style={tw`mt-16 mb-2`}>
                <Text quicksandSemibold style={tw`text-3xl my-1`}>Transactions</Text>
            </ViewM5>
            {/* <ViewM5 style={tw`mt-3 mb-28 rounded-lg bg-white`}>
                {
                    transactions.length > 0 ?
                        transactions.map((item, index) => {
                            return <Transaction
                                key={item.id + '-' + index}
                                isLast={index === (transactions.length - 1)}
                                item={item}
                                onPress={onPressItem}
                            />
                        }) :
                        <Text style={tw`text-center text-gray-400 p-5`}>No Data Yet.</Text>
                }
            </ViewM5> */}
            {
                Object.keys(groups).map((date, index) => {
                    const _transactions = groups[date] || []
                    return <Fragment key={date}>
                        <ViewM5>
                            <Text quicksandSemibold style={tw`text-base text-gray-700`}>{getDate(new Date(date))}</Text>
                        </ViewM5>
                        <ViewM5 style={tw`mt-3 ${Object.keys(groups).length - 1 === index ? 'mb-28' : 'mb-5'} rounded-lg bg-white`}>
                            {
                                _transactions.map((item, index) => {
                                    return <Transaction
                                        key={item.id + '-' + index}
                                        isLast={index === (_transactions.length - 1)}
                                        item={item}
                                        onPress={onPressItem}
                                        withoutDate
                                    />
                                })
                            }
                        </ViewM5>
                    </Fragment>
                })
            }
        </ScrollView>
        <FloatingButton onPress={() => navigation.navigate('TransactionCreate')} />
    </SafeAreaView>
}

export default Index