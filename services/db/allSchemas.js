export const CATEGORY_SCHEMA = "categories"

export const categoriesSchema = {
   name: CATEGORY_SCHEMA,
   primaryKey: 'id',
   properties: {
      id: 'string',
      name: 'string',
      transactionType: 'string',
   }
}


export const TRANSACTION_SCHEMA = "transactions"

export const transactionsSchema = {
   name: TRANSACTION_SCHEMA,
   primaryKey: 'id',
   properties: {
      id: 'string',
      type: 'string',
      amount: 'int',
      note: 'string?',
      category: CATEGORY_SCHEMA,
      date: 'date',
   }
}

export const USER_SCHEMA = 'user'

export const userSchema = {
   name: USER_SCHEMA,
   primaryKey: 'id',
   properties: {
      id: 'string',
      name: 'string',
   }
}