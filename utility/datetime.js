export const getTime = (datetime) => {
   const d = new Date(datetime);
   const hour = d.getHours();
   let minute = d.getMinutes();
   if (minute < 10) {
      minute = "0" + minute;
   }
   return `${hour}:${minute}`;
}

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
export const getDate = (datetime, forItem = false) => {
   const d = new Date(datetime);
   const day = days[d.getDay()];
   const date = d.getDate();
   const month = months[d.getMonth()];
   const year = d.getFullYear();

   if (date === new Date().getDate()) {
      return 'Today';
   }
   if (forItem) {
      return `${day}, ${date} ${month}`;
   }
   return `${day}, ${date} ${month} ${year}`;

}