import React from 'react'
import Form from '~screens/Transaction/Form';
import { actions } from 'services/db/Transaction';
import { Alert } from 'react-native';


const Update = ({ navigation, route }) => {
    const onSave = (data) => {
        if (typeof data === 'object') {
            const { item } = route.params
            const _item = JSON.parse(item)
            actions.update({ id: _item.id, ...data })
            navigation.goBack();
        }
    }
    const onPressDelete = () => {
        const { item } = route.params
        const _item = JSON.parse(item)
        Alert.alert(
            "Delete Transaction",
            "Are you sure you want to delete this transaction?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        actions.delete(_item.id)
                        navigation.goBack()
                    },
                },
                {
                    text: "No",
                },
            ]
        )
    }
    return <Form
        onPressBack={() => navigation.goBack()}
        onPressDelete={onPressDelete}
        onSave={onSave}
        data={route.params?.item}
        title="Update Transaction"
        isUpdate
    />
}

export default Update