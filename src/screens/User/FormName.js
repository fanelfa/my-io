import React, { useState } from 'react'
import { TextInput, TouchableOpacity, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import Text from '~component/Text'

const FormName = ({
    onSave = (name) => { }
}) => {
    const [name, setName] = useState('')
    const _onSave = () => {
        if (name.length > 0) {
            onSave(name)
        }
    }
    return <View style={tw`px-5 py-4 w-full rounded-lg`}>
        <Text quicksandSemibold style={tw`text-2xl text-center text-gray-500 mb-5 mt-16`}>How can I call you ?</Text>
        <TextInput
            style={tw`border border-gray-200 rounded-lg px-5 text-2xl mb-5 bg-white text-center text-gray-800`}
            onChangeText={setName}
            value={name}
            autoFocus
        />
        <TouchableOpacity
            activeOpacity={.7}
            style={tw`p-2 items-center bg-blue-500 rounded-lg`}
            onPress={_onSave}
        >
            <Text quicksandSemibold style={tw`text-xl text-white`}>Save</Text>
        </TouchableOpacity>
    </View>
}

export default FormName