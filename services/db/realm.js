const Realm = require('realm');

import { categoriesSchema, transactionsSchema, userSchema } from 'services/db/allSchemas';

const dbConfig = {
   path: 'myioRealmDB.realm',
   schema: [categoriesSchema, transactionsSchema, userSchema],
   schemaVersion: 0,
}

export const openRealm = () => {
   return Realm.open(dbConfig)
}

export const realm = new Realm(dbConfig)