import React, { useEffect, useState } from 'react'
import { useNavigation } from '@react-navigation/core'
import { Image, SafeAreaView } from 'react-native'
import tw from 'tailwind-react-native-classnames'

import Logo from 'assets/images/logoMyIO.png'
import FormName from '~screens/User/FormName'
import { actions } from 'services/db/User'

const Splash = () => {
    const navigation = useNavigation()
    const [hasName, setHasName] = useState(true)
    useEffect(() => {
        const user = actions.get()
        setTimeout(() => {
            if (user?.name) {
                navigation.replace('Home')
            } else {
                setHasName(false)
            }
        }, 1000)
    }, [])

    const onSave = (name) => {
        actions.create({ name });
        navigation.replace('Home')
    }

    return <SafeAreaView style={tw`flex-1 justify-center items-center`}>
        <Image source={Logo} style={hasName ? tw`w-48 h-48` : tw`w-36 h-36`} />
        {
            !hasName && <FormName onSave={onSave} />
        }
    </SafeAreaView>
}

export default Splash