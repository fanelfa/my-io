/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Background from '~layout/Background';
import Home from '~screens/Home';
import Splash from '~screens/Splash';

import TransactionIndex from '~screens/Transaction/Index';
import TransactionCreate from '~screens/Transaction/Create';
import TransactionUpdate from '~screens/Transaction/Update';
import CreateCategory from '~screens/Category/Create';
import { actions } from 'services/db/Category';
const Stack = createNativeStackNavigator();

const App = () => {
	// const isDarkMode = useColorScheme() === 'dark';

	// const backgroundStyle = {
	// 	backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
	// };

	useEffect(() => {
		// Category.createTable();
		// Transaction.createTable();
		actions.initCategories();
	}, [])

	// <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
	return (
		<Background>
			<NavigationContainer>
				<Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown: false }}>
					<Stack.Screen
						name="Splash"
						component={Splash}
					/>
					<Stack.Screen
						name="Home"
						component={Home}
					/>
					<Stack.Screen
						name="TransactionIndex"
						component={TransactionIndex}
						options={{ animation: "slide_from_bottom" }}
					/>
					<Stack.Screen
						name="TransactionCreate"
						component={TransactionCreate}
						options={{ animation: "slide_from_bottom" }}
					/>
					<Stack.Screen
						name="TransactionUpdate"
						component={TransactionUpdate}
						options={{ animation: "slide_from_bottom" }}
					/>
					<Stack.Screen
						name="CreateCategory"
						component={CreateCategory}
						options={{ animation: "slide_from_bottom" }}
					/>
				</Stack.Navigator>
			</NavigationContainer>
		</Background>
	);
};

export default App;
