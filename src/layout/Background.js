import React from 'react'
import { View } from 'react-native'

const Background = (props) => {
   return <View style={{
      flex: 1,
      backgroundColor: '#F7FBFC',
   }}>
      {props.children}
   </View>
}

export default Background