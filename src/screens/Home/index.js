import React, { useEffect, useState } from 'react'
import { SafeAreaView, ScrollView, TouchableOpacity, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'

import ViewM5 from '~component/ViewM5'
import Text from '~component/Text'
import FloatingButton from '~component/FloatingButton'
import Transaction from '~component/Transaction/Item'
import { useIsFocused, useNavigation } from '@react-navigation/core'
import { actions } from 'services/db/Transaction'
import { actions as userActions } from 'services/db/User'
import { TRANSACTION_TYPE } from 'utility/GlobalConst'
import { formatNumber } from 'utility/number'

const Home = () => {
   const navigation = useNavigation()

   const isFocused = useIsFocused()
   const [lastestTransactions, setLastestTransactions] = useState([])
   const [balance, setBalance] = useState(0);
   const [name, setName] = useState('Dear')

   useEffect(() => {
      if (isFocused) {
         const transactions = [...actions.getAll() || []]
         setLastestTransactions(transactions.slice(0, 5))

         let total = 0;
         transactions.forEach(transaction => {
            const amount = transaction.amount || 0
            if (transaction.type === TRANSACTION_TYPE.income) {
               total += amount
            } else {
               total -= amount
            }
         })
         setBalance(total)

         const user = userActions.get()
         if (user?.name) {
            setName(user.name)
         }
      } else {
         setLastestTransactions([])
      }
   }, [isFocused])

   const onPressItem = (item) => {
      navigation.navigate('TransactionUpdate', {
         // item: {
         //    ...item,
         //    category: JSON.stringify(item.category),
         //    date: item.date.toLocaleString(),
         // }
         item: JSON.stringify(item)
      });
   }
   const onPressSeeAll = () => {
      navigation.navigate('TransactionIndex');
   }

   return <SafeAreaView style={tw`flex-1`}>
      {/* <View style={tw`w-16 h-16 rounded-full bg-blue-100 absolute right-2 top-3`}></View> */}
      <View style={tw`w-64 h-64 rounded-full bg-blue-100 absolute -left-28 top-32`}></View>
      {/* <View style={tw`w-56 h-56 rounded-full bg-blue-200 absolute -right-28 bottom-32`}></View> */}
      {/* <View style={tw`w-32 h-32 rounded-full bg-blue-200 absolute left-16 bottom-4`}></View> */}
      {/* <View style={tw`w-full h-1/2 absolute bottom-0 bg-blue-500`}></View> */}
      <ScrollView style={{
         flex: 1,
         position: 'absolute',
         left: 0, right: 0, bottom: 0, top: 0
      }}>
         <ViewM5>
            <Text style={tw`text-gray-500 mt-5`}>Welcome back,</Text>
         </ViewM5>
         <ViewM5>
            <Text quicksandSemibold style={tw`text-3xl my-1`}>{name}</Text>
         </ViewM5>
         <ViewM5 style={tw`my-3 flex-row flex-wrap justify-between px-5 py-4 rounded-lg bg-blue-500`}>
            <Text quicksandMedium style={tw`text-lg m-0 text-white`}>Balance</Text>
            <View style={tw`flex-row flex-wrap items-baseline`}>
               <Text quicksandMedium style={tw`text-white text-xl pr-1 pb-1`}>Rp</Text>
               <Text quicksandMedium style={tw`text-white text-4xl pt-3`}>{formatNumber(balance)}</Text>
            </View>
         </ViewM5>
         <ViewM5 style={tw`flex-row justify-between mt-3`}>
            <Text quicksandMedium style={tw`text-base`}>Recent Transactions</Text>
            {
               lastestTransactions.length > 0 ?
                  <TouchableOpacity
                     activeOpacity={0.6}
                     onPress={onPressSeeAll}
                  >
                     <Text quicksandBold style={tw`text-base text-blue-500`}>See All</Text>
                  </TouchableOpacity> :
                  <View></View>
            }
         </ViewM5>
         <ViewM5 style={tw`mt-3 mb-28 rounded-lg bg-white`}>
            {
               lastestTransactions.length > 0 ?
                  lastestTransactions.map((item, index) => {
                     return <Transaction
                        key={item.id + '-' + index}
                        isLast={index === (lastestTransactions.length - 1)}
                        item={item}
                        onPress={onPressItem}
                     />
                  }) :
                  <Text style={tw`text-center text-gray-400 p-5`}>{'No Data Yet :('}</Text>
            }
         </ViewM5>
      </ScrollView>
      <FloatingButton onPress={() => navigation.navigate('TransactionCreate')} />
      {/* <ModalAddTransaction visible={showModalAdd} onCancel={() => setShowModalAdd(false)} /> */}
   </SafeAreaView>
}

export default Home