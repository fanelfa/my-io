import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import Text from '~component/Text'
import { getDate } from 'utility/datetime'
import { TRANSACTION_TYPE } from 'utility/GlobalConst'
import { formatNumber } from 'utility/number'

const defaultItem = {
   amount: 0,
   type: TRANSACTION_TYPE.income,
   note: '',
   datetime: new Date(),
   category: {},
}

const Transaction = ({
   isLast = false,
   item = defaultItem,
   onPress = (item) => { },
   withoutDate = false,
}) => {

   const label = item.note?.length > 0 ? item.note : (item.category?.name || '');
   return <TouchableOpacity
      activeOpacity={.5}
      style={tw`px-4 py-3 flex-row items-center justify-center ${isLast ? '' : ' border-b'} border-gray-200`}
      onPress={() => onPress(item)}
   >
      <View style={tw`w-7 h-7 justify-center items-center bg-blue-200 rounded`}>
         <Text quicksandBold style={tw`text-blue-500`}>{(item.category?.name || item.note).charAt(0)}</Text>
      </View>
      <View style={tw`ml-3 flex-1 flex-row flex-wrap items-center`}>
         <View>
            <Text quicksandMedium numberOfLines={1} style={tw`text-base capitalize`}>{label}</Text>
            {
               !withoutDate && <Text quicksand style={tw`text-sm text-gray-400`}>
                  {getDate(item.date)}
               </Text>
            }
         </View>
         <Text quicksandMedium style={tw`ml-auto text-2xl text-right ${item.type === TRANSACTION_TYPE.income ? 'text-green-500' : 'text-red-400'}`}>
            {item.type === TRANSACTION_TYPE.income ? '+' : '-'} Rp {formatNumber(item.amount)}
         </Text>
      </View>
   </TouchableOpacity>
}

export default Transaction