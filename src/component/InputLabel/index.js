import React from 'react'
import tw from 'tailwind-react-native-classnames'
import Text from '~component/Text'

const InputLabel = ({ name = '' }) => <Text quicksandSemibold style={tw`text-base my-2 text-gray-500`}>{name}</Text>

export default InputLabel