import React from 'react'
import { Text as RNText } from 'react-native'

const Text = ({
   style = {},
   children,
   quicksandLight = false,
   quicksand = false,
   quicksandMedium = false,
   quicksandSemibold = false,
   quicksandBold = false,
   numberOfLines
}) => {
   const getFamily = () => {
      switch (true) {
         case quicksandLight: return 'Quicksand-Light';
         case quicksand: return 'Quicksand-Regular';
         case quicksandMedium: return 'Quicksand-Medium';
         case quicksandSemibold: return 'Quicksand-SemiBold';
         case quicksandBold: return 'Quicksand-Bold';
         default: return null;
      }
   }
   return <RNText
      style={{ ...style, fontFamily: getFamily(), }}
      numberOfLines={numberOfLines}
   >{children}</RNText>
}

export default Text