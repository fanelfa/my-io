import React, { useEffect, useState } from 'react'
import { ScrollView, TextInput, TouchableOpacity, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import CurrencyInput from 'react-native-currency-input'
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Feather';
import Text from '~component/Text';
import ViewM5 from '~component/ViewM5';
import { getDate } from 'utility/datetime';
import { TRANSACTION_TYPE } from 'utility/GlobalConst';
import InputLabel from '~component/InputLabel';
import ModalChooseCategory from '~component/Modal/ChooseCategory';
import BackButtonHeader from '~component/BackButtonHeader';


const Form = ({
   title = 'Add Transaction',
   data,
   onSave = (data) => { },
   onPressBack = () => { },
   onPressDelete = () => { },
   isUpdate = false,
}) => {
   const [isIncome, setIsIncome] = useState(true);
   const [amount, setAmount] = useState();
   const [note, setNote] = useState('');
   const [category, setCategory] = useState();
   const [date, setDate] = useState(new Date());
   const [showDatePicker, setShowDatePicker] = useState(false);
   const [showModalChooseCategory, setShowModalChooseCategory] = useState(false);

   useEffect(() => {
      if (data?.length > 0) {
         const item = JSON.parse(data)
         setIsIncome(item.type === TRANSACTION_TYPE.income);
         setAmount(item.amount);
         setNote(item.note);
         setDate(new Date(item.date));
         setCategory(item.category);
      }
   }, [data])

   const onChangeDate = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      setShowDatePicker(false);
   };

   const _onSave = () => {
      if (amount && category) {
         onSave({
            type: isIncome ? TRANSACTION_TYPE.income : TRANSACTION_TYPE.expense,
            amount,
            note,
            category,
            date,
         })
      }
   }

   return <View style={tw`flex-1`}>
      <View style={tw`${isIncome ? 'bg-green-400' : 'bg-red-400'} h-64`}></View>
      <ScrollView style={{
         flex: 1,
         position: 'absolute',
         left: 0, right: 0, bottom: 0, top: 0
      }}>
         <ViewM5 style={tw`mt-8 flex-row flex-wrap items-center`}>
            <Text quicksandSemibold style={tw`flex-1 text-3xl my-1 text-white`}>{title}</Text>
            {
               isUpdate && <TouchableOpacity
                  style={tw`p-2 mr-auto`}
                  onPress={onPressDelete}
               >
                  <Icon name="trash-2" style={tw`text-white text-2xl`} />
               </TouchableOpacity>
            }
         </ViewM5>
         <ViewM5 style={tw`mt-10 mb-5 rounded-xl p-2 bg-white flex-row`}>
            <TouchableOpacity
               style={tw`flex-1 items-center p-2 bg-white ${isIncome ? 'bg-green-400' : ''} rounded-lg`}
               onPress={() => {
                  setIsIncome(true)
                  setCategory()
               }}
            >
               <Text quicksandSemibold style={tw`text-lg ${isIncome ? 'text-white' : 'text-gray-400'}`}>Income</Text>
            </TouchableOpacity>
            <TouchableOpacity
               style={tw`ml-3 flex-1 items-center p-2 bg-white ${!isIncome ? 'bg-red-400' : ''} rounded-lg`}
               onPress={() => {
                  setIsIncome(false)
                  setCategory()
               }}
            >
               <Text quicksandSemibold style={tw`text-lg ${!isIncome ? 'text-white' : 'text-gray-400'}`}>Expense</Text>
            </TouchableOpacity>
         </ViewM5>
         <ViewM5 style={tw`mb-10 rounded-xl px-3 py-5 bg-white`}>
            <InputLabel name="Amount" />
            <CurrencyInput
               style={tw`border bg-white ${isIncome ? 'border-green-200 text-green-500' : 'border-red-200 text-red-500'} rounded-lg px-5 py-2 text-2xl`}
               value={amount}
               onChangeValue={setAmount}
               precision={0}
               prefix="Rp "
               textAlign="right"
               minValue={0}
            />
            {/* <InputLabel name="Category" />
            <TextInput
               style={tw`border rounded-lg px-5 text-lg mb-1 bg-white ${isIncome ? 'border-green-200 text-green-500' : 'border-red-200 text-red-500'}`}
               onChangeText={setCategory}
               value={category}
            /> */}
            <InputLabel name="Category" />
            <TouchableOpacity
               onPress={() => setShowModalChooseCategory(true)}
               activeOpacity={.8}
               style={[tw`flex-row justify-between items-center border rounded-lg px-5 mb-1 bg-white ${isIncome ? 'border-green-200' : 'border-red-200'}`, { paddingVertical: 10 }]}
            >
               <Text style={tw`text-lg ${isIncome ? 'text-green-500' : 'text-red-500'}`} >
                  {category?.name}
               </Text>
            </TouchableOpacity>
            <InputLabel name="Date" />
            <TouchableOpacity
               onPress={() => setShowDatePicker(true)}
               activeOpacity={.8}
               style={[tw`flex-row justify-between items-center border rounded-lg px-5 mb-1 bg-white ${isIncome ? 'border-green-200' : 'border-red-200'}`, { paddingVertical: 10 }]}
            >
               <Text style={tw`text-lg ${isIncome ? 'text-green-500' : 'text-red-500'}`} >
                  {getDate(date)}
               </Text>
               <Icon name="calendar" style={tw`text-xl text-gray-700`} />
            </TouchableOpacity>
            <InputLabel name="Note" />
            <TextInput
               placeholder="ex: Buy stock"
               style={tw`border rounded-lg px-5 text-lg mb-1 bg-white ${isIncome ? 'border-green-200 text-green-500' : 'border-red-200 text-red-500'}`}
               onChangeText={setNote}
               value={note}
            />
            <View style={tw`mt-14 mb-5 bg-white flex-row`}>
               <TouchableOpacity
                  activeOpacity={.7}
                  style={tw`p-2 flex-1 items-center bg-blue-500 rounded-lg`}
                  onPress={_onSave}
               >
                  <Text quicksandSemibold style={tw`text-xl text-white`}>Save</Text>
               </TouchableOpacity>
               <TouchableOpacity
                  activeOpacity={.5}
                  onPress={onPressBack}
                  style={tw`ml-3 p-2 flex-1 items-center bg-white rounded-lg border-2 border-gray-100`}
               >
                  <Text quicksandSemibold style={tw`text-xl text-gray-700`}>Cancel</Text>
               </TouchableOpacity>
            </View>
         </ViewM5>
      </ScrollView>
      {
         showDatePicker && <DateTimePicker
            value={date}
            mode="date"
            display="default"
            onChange={onChangeDate}
            maximumDate={new Date()}
         />
      }
      <ModalChooseCategory
         visible={showModalChooseCategory}
         onCancel={() => setShowModalChooseCategory(false)}
         isIncome={isIncome}
         onSelect={setCategory}
      />
   </View>
}

export default Form