import uuid from 'react-native-uuid'

import { TRANSACTION_SCHEMA } from "./allSchemas"
import { realm } from './realm'

export const actions = {
   getAll: () => {
      return realm.objects(TRANSACTION_SCHEMA).filtered('TRUEPREDICATE SORT(date DESC)')
   },
   getAllWithLimit: (limit = 5) => {
      return realm.objects(TRANSACTION_SCHEMA).filtered(`TRUEPREDICATE SORT(date DESC) LIMIT(${limit})`)
   },
   create: ({ type, amount, note, category, date }) => {
      realm.write(() => {
         realm.create(TRANSACTION_SCHEMA, {
            id: uuid.v4(), type, amount, note, category, date: date || new Date(),
         })
      })
   },
   update: ({ id, type, amount, note, category, date }) => {
      realm.write(() => {
         const trans = realm.objects(TRANSACTION_SCHEMA).filtered(`id="${id}"`)[0]
         trans.type = type
         trans.amount = amount
         trans.note = note
         trans.category = category
         trans.date = date || new Date()
      })
   },
   delete: (id) => {
      realm.write(() => {
         const trans = realm.objects(TRANSACTION_SCHEMA).filtered(`id="${id}"`)[0]
         trans && realm.delete(trans)
      })
   }
}