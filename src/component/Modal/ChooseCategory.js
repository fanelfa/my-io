import React, { useEffect, useState } from 'react'
import { Modal, ScrollView, TouchableOpacity, View } from 'react-native'
// import { useNavigation } from '@react-navigation/core'
import { actions } from 'services/db/Category'
import tw from 'tailwind-react-native-classnames'
import { TRANSACTION_TYPE } from 'utility/GlobalConst'
import Text from '~component/Text'

const ModalChooseCategory = ({
   visible, isIncome = false,
   onSelect = (category) => { }, onCancel = () => { },
}) => {
   // const navigation = useNavigation();

   const [categories, setCategories] = useState([]);

   useEffect(() => {
      const filterType = isIncome ? TRANSACTION_TYPE.income : TRANSACTION_TYPE.expense;
      visible && setCategories((actions.getAll() || []).filter(val => val.transactionType === filterType))
   }, [visible, isIncome])

   return <Modal
      visible={visible} animationType="slide"
      transparent
   >
      <View style={tw`flex-1 bg-gray-600 bg-opacity-50`}>
         <View style={tw`flex-1 m-3 bg-white rounded-xl`}>
            <Text quicksandBold style={tw`p-3 text-xl text-center text-white bg-blue-500 rounded-t-lg`}>Choose Category</Text>
            <ScrollView style={tw`flex-1 rounded-lg px-3`}>
               {
                  categories.map((category, index) => {
                     const isLast = categories.length - 1 === index;
                     return <TouchableOpacity
                        key={index}
                        onPress={() => {
                           onSelect(category);
                           setTimeout(() => {
                              onCancel();
                           }, 200)
                        }}
                        style={tw`border-b-2 border-gray-100 py-3 px-5`}
                     >
                        <Text quicksandMedium style={tw`text-xl text-gray-700`}>{category.name}</Text>
                     </TouchableOpacity>
                  })
               }
               {/* <TouchableOpacity
                  activeOpacity={.5}
                  onPress={() => {
                     navigation.navigate('CreateCategory');
                  }}
                  style={tw`p-2 my-3 items-center bg-white rounded-sm border-2 border-dotted border-blue-50`}
               >
                  <Text quicksandSemibold style={tw`text-xl text-blue-400`}>Add Category</Text>
               </TouchableOpacity> */}
            </ScrollView>
            <TouchableOpacity
               activeOpacity={.5}
               onPress={onCancel}
               style={tw`p-2 m-3 items-center bg-white rounded-lg border-2 border-gray-100`}
            >
               <Text quicksandSemibold style={tw`text-xl text-gray-700`}>Cancel</Text>
            </TouchableOpacity>
         </View>
      </View>
   </Modal>
}

export default ModalChooseCategory