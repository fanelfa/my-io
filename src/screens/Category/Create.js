import React from 'react'
import Form from '~screens/Category/Form'
import { actions } from 'services/db/Category';

const Create = ({ navigation }) => {
   const onSave = (data) => {
      if (typeof data === 'object') {
         actions.create({
            name: data.name,
            transactionType: data.transactionType,
         })
      }
   }
   return <Form onPressBack={() => navigation.goBack()} onSave={onSave} />
}

export default Create