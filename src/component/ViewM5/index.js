import React from 'react'
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';

const ViewM5 = ({ style, children, ...props }) => {
   return <View style={{ ...tw`mx-5`, ...style, }} {...props}>{children}</View>;
}

export default ViewM5